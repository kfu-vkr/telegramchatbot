using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace TelegramChatBotExperiment
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface IChatBotAI
    {
        public bool AIReady { get; }

        /// <summary>
        /// Инициализация Pullenti SDK
        /// </summary>
        /// <param name="verbose"></param>
        public void Initialize(bool verbose = default);

        /// <summary>
        /// Получение список нормальных форм существительных входной строки sourceString
        /// </summary>
        /// <param name="sourceString">Входная строка</param>
        /// <returns>Список нормальных форм <see cref="NFNounGroup"/></returns>
        public IEnumerable<NFNounGroup> GetNfNounGroups(string sourceString);
    }
}