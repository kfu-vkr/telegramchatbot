using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace TelegramChatBotExperiment
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "MethodOverloadWithOptionalParameter")]
    public class NFNounGroup
    {
        public string TokenSource { get; init; }
        
        // Единственное число
        public string NFSingular { get; init; }
        
        // Плюральная форма
        public string NFPlular { get; init; }
        
        // Неопределенная форма
        public string NFUndefined { get; init; }

        public override string ToString()
        {
            return ToString(true);
        }

        public string ToString(bool NeedSingular = default, bool NeedPlural = default, bool NeedUndefined = default)
        {
            var sb = new StringBuilder();
            sb.Append('[');
            sb.Append(TokenSource);
            
            if (NeedSingular == NeedPlural == NeedUndefined == false)
            {
                sb.Append(']');
                return sb.ToString();
            }

            if (NeedSingular)
            {
                sb.Append(" => ед.ч:");
                sb.Append(NFSingular);
            }
            
            if (NeedPlural)
            {
                sb.Append(" => мн.ч:");
                sb.Append(NFPlular);
            }
            
            if (NeedUndefined)
            {
                sb.Append(" => неопр:");
                sb.Append(NFUndefined);
            }
            
            sb.Append(']');
            return sb.ToString();
        }


    }
}