﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace TelegramChatBotExperiment
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    internal class Program
    {
        const string EnvironmentVariableName = "ASPNETCORE_ENVIRONMENT";
        
        private static async Task Main()
        {
            var serviceProvider = ConfigureServices();
            var app = serviceProvider.GetRequiredService<ChatBot>();
            await app.Run();
        }

        private static IConfiguration InitConfiguration()
        {
            var environmentName = 
                Environment.GetEnvironmentVariable(EnvironmentVariableName);
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory)?.FullName)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{environmentName}.json", true, true)
                .AddEnvironmentVariables()
                .Build();
            return configuration;
        }
        
        private static IServiceProvider ConfigureServices(IServiceCollection services = default)
        {
            services ??= new ServiceCollection();

            // var environmentName = 
            //     Environment.GetEnvironmentVariable(EnvironmentVariableName);
            // var configuration = new ConfigurationBuilder()
            //     .AddJsonFile("appsettings.json", true, true)
            //     .AddJsonFile($"appsettings.{environmentName}.json", true, true)
            //     .AddEnvironmentVariables()
            //     .Build();

            var configuration = InitConfiguration();
            services.AddSingleton(configuration);
            
            services.AddLogging(configure =>
            {
                configure.AddConsole();
            });

            services.AddSingleton<IChatBotAI>(provider =>
                new ChatBotAI(
                    provider.GetRequiredService<ILogger<ChatBotAI>>(),
                    true, true)
            );

            services.AddTransient<ChatBot>();

            return services.BuildServiceProvider();
        }
    }
}