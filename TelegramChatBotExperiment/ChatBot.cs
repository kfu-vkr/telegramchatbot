using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace TelegramChatBotExperiment
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class ChatBot
    {
        private readonly ILogger _logger;
        private readonly IChatBotAI _chatBotAi;
        private readonly IConfiguration _configuration;

        public ChatBot(ILogger<ChatBot> logger, IChatBotAI chatBotAi, IConfiguration configuration)
        {
            _logger = logger;
            _chatBotAi = chatBotAi;
            _configuration = configuration;
        }
        
        public async Task Run(CancellationToken ct = default)
        {
            // Ожидаем инициализация Pullenti
            while (!_chatBotAi.AIReady)
            {
                await Task.Delay(500, ct);
            }

            var botToken = _configuration.GetSection("AppSettings:TgBotApiToken").Get<string>();
            var pollTimeout = _configuration.GetSection("AppSettings:PollTimeoutMsec").Get<int>();
            
            var botClient = new TelegramBotClient(botToken);
            var wh = await botClient.GetWebhookInfoAsync(ct);
            // Удаляем старый WebHook, если есть
            if (!string.IsNullOrEmpty(wh.Url))
            {
                await botClient.SetWebhookAsync("", cancellationToken: ct);
            }

            var updateOffset = 0;
            while (!ct.IsCancellationRequested)
            {
                var rawUpdates = await
                    botClient.GetUpdatesAsync(updateOffset, 
                        timeout: pollTimeout, 
                        allowedUpdates: new[]{ UpdateType.Message },
                        cancellationToken: ct);
                if (rawUpdates == null || !rawUpdates.Any())
                {
                    continue;
                }
                
                var lastUpdate = rawUpdates.Last();
                updateOffset = lastUpdate.Id + 1;

                foreach (var update in rawUpdates)
                {
                    if (string.IsNullOrEmpty(update.Message.Text?.Trim()))
                    {
                        continue;
                    }
                
                    var msgText = update.Message.Text.Trim();

                    _logger.LogInformation($"Исходное сообщение: {msgText}");

                    var nounGroups = _chatBotAi.GetNfNounGroups(msgText)
                        .ToArray();
                    var nouns = nounGroups.Select(x =>
                            x.ToString(true, true, true))
                        .ToArray();
                    _logger.LogInformation($"Группы существительных:{Environment.NewLine}" + 
                                           $"{string.Join(Environment.NewLine, nouns)}");

                    if (!nounGroups.Any())
                    {
                        continue;
                    }
                    
                    var msg = new StringBuilder();
                    msg.AppendLine("Исходное слово или предложение:");
                    msg.AppendLine($"<code>{msgText}</code>");
                    msg.AppendLine();
                    msg.AppendLine("Найдены группы существительных:");
                    msg.AppendLine();
                    msg.Append("<code>");
                    foreach (var word in nounGroups)
                    {
                        msg.AppendLine($"{word.TokenSource} => {word.NFSingular}");
                    }
                    msg.AppendLine("</code>");
                    
                    await botClient.SendTextMessageAsync(update.Message.Chat, msg.ToString(), 
                        ParseMode.Html, cancellationToken: ct);
                }
            }
        }
    }
}