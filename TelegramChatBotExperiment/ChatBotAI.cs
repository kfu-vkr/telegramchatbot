using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.Extensions.Logging;
using Pullenti.Morph;
using Pullenti.Ner;
using Pullenti.Ner.Core;

namespace TelegramChatBotExperiment
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class ChatBotAI : IChatBotAI
    {
        private readonly ILogger _logger;
        public bool AIReady { get; private set; }

        public ChatBotAI(ILogger<ChatBotAI> logger, bool? doInit = default, bool? verbose = default)
        {
            _logger = logger;

            if (doInit == true)
            {
                Initialize(verbose ?? false);
            }
        }

        public void Initialize(bool verbose = default)
        {
            if (verbose)
            {
                _logger.LogInformation($"Инициализация Pullenti SDK (версия {Pullenti.Sdk.Version} " + 
                                       $"({Pullenti.Sdk.VersionDate}) ... ");
            }
            
            Pullenti.Sdk.InitializeAll();

            if (verbose)
            {
                var aList = ProcessorService.Analyzers.Select(analyzer =>
                    $"{(analyzer.IsSpecific ? "S" : "C")}:{analyzer.Name} ({analyzer.Caption})").ToList();
                _logger.LogInformation($"Доступны следующие анализиторы: {string.Join(", ", aList)}");
            }
            
            AIReady = true;
        }

        public IEnumerable<NFNounGroup> GetNfNounGroups(string sourceString)
        {
            // Запускаем обработку на пустом процессоре (без анализаторов NER)
            var analysisResult = ProcessorService.EmptyProcessor.Process(new SourceOfAnalysis(sourceString));

            // Возвращаемый список нормализованных форм существительных
            var nounGroups = new List<NFNounGroup>();
                    
            for (var t = analysisResult.FirstToken; t != null; t = t.Next) 
            {
                // нетекстовые токены и несуществительные игнорируем
                if (t is not TextToken || !t.Morph.Class.IsNoun)
                {
                    continue;
                }
                
                // Выделяем именную группу с текущего токена
                var npt = NounPhraseHelper.TryParse(t);
                        
                // Не получилось
                if (npt == null)
                {
                    continue;
                }

                nounGroups.Add(new NFNounGroup
                {
                    TokenSource = npt.GetSourceText(),
                    NFSingular = npt.GetNormalCaseText(null, MorphNumber.Singular),
                    NFPlular = npt.GetNormalCaseText(null, MorphNumber.Plural),
                    NFUndefined = npt.GetNormalCaseText(),
                });
                        
                // Указатель на последний токен именной группы
                t = npt.EndToken;
            }

            return nounGroups;
        }
    }
}